package com.study.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.study.domain.po.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

//    插入
    @Test
    void testInsert() {
        User user = new User();
        user.setId(5L);
        user.setUsername("Lucy");
        user.setPassword("123");
        user.setPhone("18688990011");
        user.setBalance(200);
        user.setInfo("{\"age\": 24, \"intro\": \"英文老师\", \"gender\": \"female\"}");
        user.setCreateTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());
        userMapper.insert(user);
    }

//    查询单个
    @Test
    void testSelectById() {
        User user = userMapper.selectById(2L);
        System.out.println("user = " + user);
    }

//查询多个
    @Test
    void testQueryByIds() {

        List<User> users = userMapper.selectBatchIds(List.of(1L, 2L, 3L, 4L));
        users.forEach(System.out::println);
    }
//更新
    @Test
    void testUpdateById() {
        User user = new User();
        user.setId(5L);
        user.setBalance(20000);
        userMapper.updateById(user);
    }
//删除
    @Test
    void testDeleteUser() {
        userMapper.deleteById(5L);
    }

//    查询出名字中带o的，存款大于等于1000元的人的id、username、info、balance字段
//    SELECT id,username,info,balance FROM user WHERE username LIKE ? AND balance >= ?
    @Test
    void testquerywrapper() {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<User>()
                .select("username","info")
                .like("username","o")
                .ge("balance",200);
        List<User> users = userMapper.selectList(userQueryWrapper);
        users.forEach(System.out::println);

    }

//    更新用户名为jack的用户的余额为2000
//    UPDATE user     SET balance = 2000     WHERE (username = "jack")

    @Test
    void testupdatewrapper() {
        User user = new User();
        user.setBalance(2000);
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<User>()
                .eq("username","jack");
        int result = userMapper.update(user, userQueryWrapper);
        System.out.println(result);

    }
//    更新id为1,2,4的用户的余额，扣200
//UPDATE user     SET balance = balance - 200     WHERE id in (1, 2, 4)
    @Test
    void testupdatewrapper1() {

        UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<User>()
                .setSql("balance=balance-200")
                .in("id",List.of(1L,2L,3L));

        int result = userMapper.update(null, userUpdateWrapper);
        System.out.println(result);

    }

//    LambdaQueryWrapper使用
    @Test
    void testlambdaquerywrapper() {
        LambdaQueryWrapper<User> userQueryWrapper = new LambdaQueryWrapper<User>()
                .select(User::getId,User::getInfo)
                .like(User::getUsername,"o")
                .ge(User::getBalance,200);
        List<User> users = userMapper.selectList(userQueryWrapper);
        users.forEach(System.out::println);
    }

//自定义SQL  查询出所有收货地址在北京的并且用户id在指定范围的用户（例如1、2、4 ）的用户
    @Test
    void testzidingyiwrapper1() {

        List<Long> ids=List.of(1L,2L,3L);
        int amount=200;
        UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<User>()
                .in("id",ids);

//        自定义方法
        int result = userMapper.updateByIdzidy( userUpdateWrapper,amount);
        System.out.println(result);

    }












}