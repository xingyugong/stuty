package com.study.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.study.domain.dto.UserFormDTO;
import com.study.domain.po.User;
import com.study.domain.query.UserQuery;
import com.study.domain.vo.UserVO;
import com.study.service.IUserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/user")
@RestController
//对初始化的变量进行构造 如userService
@RequiredArgsConstructor
public class UserController {


    private final IUserService userService;



    @PostMapping
    @ApiOperation("新增用户")
    public void saveUser(@RequestBody UserFormDTO userFormDTO){
        // 1.转换DTO为PO   将userFormDTO属性复制到User  前--后
        User user = BeanUtil.copyProperties(userFormDTO, User.class);
        // 2.新增
        userService.save(user);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除用户")
    public void removeUserById(@PathVariable("id") Long userId){
        userService.removeById(userId);
    }

//    @GetMapping("/{id}")
//    @ApiOperation("根据id查询用户")
//    public UserVO queryUserById(@PathVariable("id") Long userId){
//        // 1.查询用户
//        User user = userService.getById(userId);
//        // 2.处理vo
//        return BeanUtil.copyProperties(user, UserVO.class);
//    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查询用户")
    public UserVO queryUserById(@PathVariable("id") Long userId){

        return userService.queryUserAndAddressById(userId);
    }

//    @GetMapping
//    @ApiOperation("根据id集合查询用户")
//    public List<UserVO> queryUserByIds(@RequestParam("ids") List<Long> ids){
//        // 1.查询用户
//        List<User> users = userService.listByIds(ids);
//        // 2.处理vo
//        return BeanUtil.copyToList(users, UserVO.class);
//    }
    @GetMapping
    @ApiOperation("根据id集合查询用户")
    public List<UserVO> queryUserByIds(@RequestParam("ids") List<Long> ids){
        return userService.queryUserAndAddressByIds(ids);
    }
//    @PutMapping("{id}/deduction/{money}")
//    @ApiOperation("扣减用户余额")
//    public void deductBalance(@PathVariable("id") Long id, @PathVariable("money")Integer money){
//        userService.deductBalance(id, money);
//    }

//    扣减后余额为0，则将用户status修改为冻结状态
    @PutMapping("{id}/deduction/{money}")
    @ApiOperation("扣减用户余额，修改状态")
    public void deductBalance1(@PathVariable("id") Long id, @PathVariable("money")Integer money){
        userService.deductBalance1(id, money);
    }


    @GetMapping("/list")
    @ApiOperation("根据复杂查询用户")
    public List<UserVO> queryUsers(UserQuery query){
        // 1.组织条件
        String username = query.getName();
        Integer status = query.getStatus();
        Integer minBalance = query.getMinBalance();
        Integer maxBalance = query.getMaxBalance();
        LambdaQueryWrapper<User> wrapper = new QueryWrapper<User>().lambda()
                .like(username != null, User::getUsername, username)
                .eq(status != null, User::getStatus, status)
                .ge(minBalance != null, User::getBalance, minBalance)
                .le(maxBalance != null, User::getBalance, maxBalance);
        // 2.查询用户
        List<User> users = userService.list(wrapper);
        // 3.处理vo
        return BeanUtil.copyToList(users, UserVO.class);
    }



}
