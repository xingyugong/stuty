package com.study.service;

import com.study.domain.po.Address;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wei
 * @since 2024-04-25
 */
public interface IAddressService extends IService<Address> {

}
