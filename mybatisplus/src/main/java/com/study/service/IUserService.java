package com.study.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.study.domain.po.User;
import com.study.domain.vo.UserVO;

import java.util.List;

public interface IUserService extends IService<User> {
    void deductBalance(Long id, Integer money);
    void deductBalance1(Long id, Integer money);

    UserVO queryUserAndAddressById(Long userId);

    List<UserVO> queryUserAndAddressByIds(List<Long> ids);
}
