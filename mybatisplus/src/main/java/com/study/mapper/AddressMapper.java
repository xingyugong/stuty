package com.study.mapper;

import com.study.domain.po.Address;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wei
 * @since 2024-04-25
 */
public interface AddressMapper extends BaseMapper<Address> {

}
